"""
    Compute the eigenvalues and eigenvectors of a real generalized symmetric-definite
    eigenproblem of the form 
        A*x = lambda*B*x

    Test the mpi implementation
"""

from mpi4py import MPI
import numpy as np
from scipy.linalg.blas import dgemv
import pyscalapack.pyscalapack as pys
from pyscalapack.tools.m_utils import g2l, scatter_matrix, scatter_vector, get_vector_index
import sys


def gemv_mpi(comm, A_mpi, B_mpi, N, nb, order="F"):
    
    # initialise blacs
    iam, nprocs = pys.blacs_pinfo()

    nprow = int(np.sqrt(1.0*nprocs))
    npcol = int(nprocs/nprow)

    context = pys.blacs_get(-1)
    pys.blacs_gridinit(context, nprow, npcol)

    nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)

    if (myrow < 0 or mycol < 0):
        raise ValueError("myrow < 0 or mycol < 0")

    # initialise temporary arrays on each thread
    myArows = pys.numroc(N, nb, myrow, 0, nprow)
    myAcols = pys.numroc(N, nb, mycol, 0, npcol)

    myBrows = pys.numroc(N, nb, myrow, 0, nprow)
    myBcols = 1


    comm.Barrier()

    # Scatter A on each thread. Probably better to use MPI_Scatter !!
    ath = scatter_matrix(A_mpi, nprow, npcol, myrow, mycol, nb, myArows, myAcols, order=order)
    desca, info = pys.descinit(N, N, nb, nb, context, myArows)
    
    # Scatter B on each thread. Probably better to use MPI_Scatter !!
    bth = scatter_vector(B_mpi, nprow, myrow, nb, myBrows, order="F")
    descb, info = pys.descinit(N, 1, nb, nb, context, myBrows)

    # output as same dim than B
    cth = np.zeros((myBrows, myBcols), dtype=np.float64, order=order)
    descc, info = pys.descinit(N, 1, nb, nb, context, myBrows)


    global_index = get_vector_index(N, nprow, myrow, nb, myBrows, dtype="i")
   
    # collect local array sizes on master node
    sendcount = np.array(comm.gather(global_index.size, 0))

    index_dist = None
    if iam ==0:
        index_dist = np.zeros([nprocs, np.max(sendcount)], dtype="i")
    comm.Gather(global_index, index_dist, root=0)

    # Perform matrix-vector multiplication
    cth = pys.pdgemv(N, N, 1.0, ath, 1, 1, desca, bth, 1, 1, descb, 1, 1, descc)

    # For some reason that I did not understood, 1 node over 2 give cth = 0
    # Nevertheless, the final results is correct, and the input array is well
    # separated between the different threads. I think that, somehow, pdgemv
    # is already gathering the results on two threads, why???

    # this test check if the returned array from pdgemv is not only constituted of zeros
    allzeros = np.allclose(cth, np.zeros(cth.shape))

    # Gather on master node
    allzeros_dist = np.array(comm.gather(allzeros, 0))
    
    # Print arrays For debugging
    #comm.Barrier()
    #for i in range(nprocs):
    #    comm.Barrier()
    #    if i == iam:
    #        print("rank = {0}: ath = \n".format(iam), ath)
    #        print("bth = \n", bth)
    #        print("cth = \n", cth)
    #        print("sendcount = \n", sendcount)
 

    # gather the result in the master thread
    if iam == 0:
        C_mpi = np.empty(N, dtype=np.float64, order=order)
        for ind, ci in enumerate(cth):
            C_mpi[index_dist[0][ind]] = ci
        for i in range(1, nprocs):
            if not allzeros_dist[i]:
                for ind, ci in enumerate(cth):
                    C_mpi[index_dist[i, ind]] = comm.recv(source=i)
    elif iam > 0:
        C_mpi = None
        # if not cth is not constituted of zeros, then send cth on the master thread
        if not allzeros:
            for ind, ci in enumerate(cth):
                comm.send(ci, dest=0)

   
    return C_mpi



comm = MPI.COMM_WORLD
rank = comm.Get_rank()

N = 4           # size of the problem (4x4 matrix)
nb = 1          # block size, only working with 1 for the moment ??
order = "F"     # For the moment working only with colum-major


A_mpi = np.zeros((N, N), dtype=np.float64, order=order)
B_mpi = np.zeros(N, dtype=np.float64, order=order)

# Initialize data
if rank == 0:
    for i in range(N):
        B_mpi[i] = 0.2*i*np.random.randn(1)[0]
        for j in range(N):
            A_mpi[i, j] = (i-0.5*j)*np.random.randn(1)[0]

    As = np.copy(A_mpi)
    Bs = np.copy(B_mpi)

    Cs = dgemv(1.0, As, Bs)
# End solving serial problem

# Bdcast arrays on each thread, we should avoid this operation for 
# production code, 
comm.Bcast(A_mpi)
comm.Bcast(B_mpi)

comm.Barrier()

# Matrix-verctor multiplications with MPI 
C_mpi = gemv_mpi(comm, A_mpi, B_mpi, N, nb, order="F")

if rank == 0:
    print("allclose: ", np.allclose(Cs, C_mpi))
