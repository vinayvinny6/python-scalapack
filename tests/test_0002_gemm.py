import unittest
from mpi4py import MPI
import numpy as np
from scipy.linalg.blas import dgemm
import pyscalapack.pyscalapack as pys
from pyscalapack.tools.m_utils import scatter_matrix, get_matrix_index, gather_matrix
import sys


def gemm_mpi(A_mpi, B_mpi, N, nb, comm, order="F"):
    # initialise mpi
    iam, nprocs = pys.blacs_pinfo()

    nprow = int(np.sqrt(1.0*nprocs))
    npcol = int(nprocs/nprow)

    context = pys.blacs_get(-1)
    pys.blacs_gridinit(context, nprow, npcol)

    nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)

    if (myrow < 0 or mycol < 0):
        raise ValueError("myrow < 0 or mycol < 0")

    myArows = pys.numroc(A_mpi.shape[0], nb, myrow, 0, nprow)
    myAcols = pys.numroc(A_mpi.shape[1], nb, mycol, 0, npcol)
    
    myBrows = pys.numroc(B_mpi.shape[0], nb, myrow, 0, nprow)
    myBcols = pys.numroc(B_mpi.shape[1], nb, mycol, 0, npcol)
    
    # This will depend of the transpose operation !!
    C_dim = np.array([A_mpi.shape[0], B_mpi.shape[1]])
    myCrows = pys.numroc(C_dim[0], nb, myrow, 0, nprow)
    myCcols = pys.numroc(C_dim[1], nb, mycol, 0, npcol)


    comm.Barrier()
    # Scatter A on each thread. Probably better to use MPI_Scatter !!
    ath = scatter_matrix(A_mpi, nprow, npcol, myrow, mycol, nb, myArows, myAcols, order=order)
    desca, info = pys.descinit(N, N, nb, nb, context, myArows)
    
    # Scatter B on each thread. Probably better to use MPI_Scatter !!
    bth = scatter_matrix(B_mpi, nprow, npcol, myrow, mycol, nb, myBrows, myBcols, order=order)
    descb, info = pys.descinit(N, N, nb, nb, context, myBrows)

    descc, info = pys.descinit(N, N, nb, nb, context, myCrows)
    
    # get conversion array index for final matrix
    global_index = get_matrix_index(C_dim, nprow, npcol, myrow, mycol, nb, myCrows, myCcols, dtype="i")

    # collect local array sizes on master node
    sendcount_i = np.array(comm.gather(global_index.shape[0], 0))
    sendcount_j = np.array(comm.gather(global_index.shape[1], 0))

    index_dist = None
    if iam == 0:
        index_dist = np.zeros([nprocs, np.max(sendcount_i), np.max(sendcount_j), 2], dtype="i")
    comm.Gather(global_index, index_dist, root=0)
 
    comm.Barrier()

    cth = pys.pdgemm(N, N, N, 1.0, ath, 1, 1, desca, bth, 1, 1, descb, 1, 1, descc)

    return gather_matrix(cth, C_dim, myCrows, myCcols, comm, index_dist)


class KnowValues(unittest.TestCase):

    def test_pdgemm(self):
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()

        N = 4       # size of the problem (4x4 matrix)
        nb = 1      # block size
        order = "F"

        A_mpi = np.zeros((N, N), dtype=np.float64, order=order)
        B_mpi = np.zeros((N, N), dtype=np.float64, order=order)

        # Initialize data
        if rank == 0:
            Cs = np.zeros((N, N), dtype=np.float64, order=order)
            for i in range(N):
                for j in range(N):
                    A_mpi[i, j] = (i-0.5*j)*np.random.randn(1)[0]
                    B_mpi[i, j] = 0.2*(i-0.5)*(j+2)*np.random.randn(1)[0]

            As = np.copy(A_mpi)
            Bs = np.copy(B_mpi)

            Cs = dgemm(1.0, As, Bs)
        # End solving serial problem

        comm.Bcast(A_mpi)
        comm.Bcast(B_mpi)

        # Same matrix-matrix mutiplication with MPI
        C_mpi = gemm_mpi(A_mpi, B_mpi, N, nb, comm, order="F")

        if rank == 0:
            assert(np.allclose(Cs, C_mpi))

if __name__ == "__main__":
    print("Testing pblas p?gemm")
    unittest.main()
